package tk.silicacraft.saoplugin;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import static java.lang.Integer.parseInt;

public class main extends JavaPlugin{
    public static main self;
    @Override
    public void onEnable(){
        self=this;
        this.getCommand("sao").setExecutor(new CommandSao(this));
        this.getCommand("col").setExecutor(new CommandCol(this));
        this.getCommand("floor").setExecutor(new CommandFloor(this));
        registerConfig();
    }

    @Override
    public void onDisable(){
        //Fired when the server stops and disables all plugins
    }

    private void registerConfig() {
        reloadConfig();
        getConfig().options().copyDefaults(true);
        saveConfig();

    }



}