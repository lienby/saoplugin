package tk.silicacraft.saoplugin;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Scanner;


public class CommandFloor implements CommandExecutor {
    private main plugin;
    public CommandFloor(main main){

        this.plugin = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {


        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (player.hasPermission("SaoPlugin.sao")) {
                // Determine what floor the player is in
                Location location = player.getLocation();
                World world = location.getWorld();
                int Floor = new Scanner(world.getName()).useDelimiter("[^\\d]+").nextInt();
                //Tell the player information
                player.sendMessage(ChatColor.GREEN+"Floor: "+Floor);
                if (Floor == 1)
                {
                    player.sendMessage(ChatColor.GREEN+"Information: The starter floor");
                    player.sendMessage(ChatColor.GREEN+"Settlements: «Town of Beginnings», «Tolbana» and «Horunka»");
                }

            }
            else
            {
                player.sendMessage(ChatColor.DARK_RED+"No permission uwu");
            }

        }

        // If the player (or console) uses our command correct, we can return true nub lol
        return true;
    }


}